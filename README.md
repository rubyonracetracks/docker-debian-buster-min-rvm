# Docker Debian Buster - Minimal - RVM

This repository is used for building the Minimal Debian Buster Docker image with RVM for [Ruby on Racetracks](https://www.rubyonracetracks.com/).

## Name of This Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-buster-min-rvm](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-rvm/container_registry)

## Upstream Docker Image
[registry.gitlab.com/rubyonracetracks/docker-debian-buster-min-stage2](https://gitlab.com/rubyonracetracks/docker-debian-buster-min-stage2/container_registry)

## What's Added
RVM

## Things NOT Included
Specific versions of Ruby

## What's the Point?
This Docker image is a building block for other Docker images for [Ruby on Racetracks](https://www.rubyonracetracks.com/).

## More Information
General information common to all Docker Debian build repositories is in the [FAQ](https://gitlab.com/rubyonracetracks/docker-debian-common/blob/master/FAQ.md).
